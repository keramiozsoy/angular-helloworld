/* globals gauge*/
"use strict";
const { openBrowser,write, closeBrowser, goto, press, text, focus, textBox, toRightOf,into,waitFor,click,link } = require('taiko');
const assert = require("assert");
const headless = process.env.headless_chrome.toLowerCase() === 'true';

beforeSuite(async () => {
    await openBrowser({ headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'] })
});

afterSuite(async () => {
    await closeBrowser();
});

step("Goto todo app", async () => {
    await goto('http://35.202.100.170');
});

step("Add Todo <query>", async (query) => {
    await write(query, into(textBox({name: "todo"})));
    await click(link({id:"addTodo"}));
    await waitFor(1000);
    await click("delete");
    await waitFor(1000);
});
